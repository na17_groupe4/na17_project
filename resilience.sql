DROP VIEW IF EXISTS vProches ;
DROP VIEW IF EXISTS vMessages ;
DROP VIEW IF EXISTS vFait_Partie ;
DROP VIEW IF EXISTS vLocalisation;
DROP VIEW IF EXISTS vUtilisateurs ;
DROP TABLE IF EXISTS SF_COMMUNAUTE;
DROP TABLE IF EXISTS SERVICE_COMMUNAUTE;
DROP TABLE IF EXISTS LIEN_COMMUNAUTE;
DROP TABLE IF EXISTS SF_PERSONNE;
DROP TABLE IF EXISTS SERVICE_PERSONNE;
DROP TABLE IF EXISTS LIEN_PERSONNE;
DROP TABLE IF EXISTS SF_SERVICE;
DROP TABLE IF EXISTS SAVOIR_FAIRE;
DROP TABLE IF EXISTS SERVICE;
DROP TABLE IF EXISTS MESSAGE;
DROP TABLE IF EXISTS VOTE;
DROP TABLE IF EXISTS MEMBRE;
DROP TABLE IF EXISTS PERSONNE;
DROP TABLE IF EXISTS COMMUNAUTE;
DROP TABLE IF EXISTS COMPTE;


CREATE TABLE COMPTE(
  cle VARCHAR(40) PRIMARY KEY,
  montant FLOAT DEFAULT 0
);

CREATE TABLE PERSONNE(
  login VARCHAR PRIMARY KEY,
  creation DATE DEFAULT DATE(NOW()),
  latitude FLOAT NOT NULL,
  longitude FLOAT NOT NULL,
  nom VARCHAR (20) NOT NULL,
  prenom VARCHAR(20) NOT NULL,
  anniversaire DATE,
  compteG1 VARCHAR(40) UNIQUE,
  FOREIGN KEY (compteG1) REFERENCES COMPTE(cle)
);


CREATE TABLE COMMUNAUTE(
  login VARCHAR PRIMARY KEY,
  nom VARCHAR (30) NOT NULL,
  description VARCHAR(100) NOT NULL,
  compteG1 VARCHAR(40) UNIQUE,
  latitude FLOAT NOT NULL,
  longitude FLOAT NOT NULL,
  creation DATE NOT NULL DEFAULT DATE(NOW())

);

CREATE TABLE MEMBRE(
  idPersonne VARCHAR,
  idCommunaute VARCHAR,
  exclu BOOLEAN,
  PRIMARY KEY (idPersonne,idCommunaute),
  FOREIGN KEY (idPersonne) REFERENCES PERSONNE(login),
  FOREIGN KEY (idCommunaute) REFERENCES COMMUNAUTE(login)
);

CREATE TABLE VOTE(
idVotant VARCHAR,
idCommunauteVotant VARCHAR,
idCible VARCHAR,
idCommunauteCible VARCHAR,
PRIMARY KEY (idVotant,idCommunauteVotant,idCible,idCommunauteCible),
FOREIGN KEY (idVotant,idCommunauteVotant) REFERENCES MEMBRE(idPersonne,idCommunaute),
FOREIGN KEY (idCible,idCommunauteCible) REFERENCES MEMBRE(idPersonne,idCommunaute),
CONSTRAINT CHK_VoteCommunaute CHECK (idCommunauteCible=idCommunauteVotant),
CONSTRAINT CHK_VotePersonne CHECK (idVotant!=idCible)
);

CREATE TABLE MESSAGE(
id  SERIAL PRIMARY KEY,
objet VARCHAR(50) NOT NULL,
contenu VARCHAR(200) NOT NULL,
dateEnvoi DATE NOT NULL DEFAULT DATE(NOW()),
expediteurP VARCHAR,
destinataireP VARCHAR,
expediteurC VARCHAR,
destinataireC VARCHAR,
reference INT,
FOREIGN KEY (expediteurP) REFERENCES PERSONNE(login),
FOREIGN KEY (destinataireP) REFERENCES PERSONNE(login),
FOREIGN KEY (expediteurC) REFERENCES COMMUNAUTE(login),
FOREIGN KEY (destinataireC) REFERENCES COMMUNAUTE(login),
CONSTRAINT CHK_Expediteur CHECK ((expediteurC IS NULL AND expediteurP IS NOT NULL) OR (expediteurC IS NOT NULL AND expediteurP IS NULL)),
CONSTRAINT CHK_Destinataire CHECK ((destinataireC IS NULL AND destinataireP IS NOT NULL) OR (destinataireC IS NOT NULL AND destinataireP IS NULL))
);

CREATE TABLE SAVOIR_FAIRE(
  id SERIAL PRIMARY KEY,
  nom VARCHAR NOT NULL
);

CREATE TABLE SERVICE(
  id SERIAL PRIMARY KEY,
  nom VARCHAR(35) NOT NULL,
  description VARCHAR(200) NOT NULL,
  type VARCHAR(2) NOT NULL,
  prix FLOAT,
  service INT,
  CONSTRAINT CHK_Service CHECK (type='s' OR type='m' OR type='sc')
);

CREATE TABLE SF_SERVICE(
  idSF INT,
  idService INT,
  PRIMARY KEY (idSF,idService),
  FOREIGN KEY (idSF) REFERENCES SAVOIR_FAIRE(id),
  FOREIGN KEY (idService) REFERENCES SERVICE(id)
);

CREATE TABLE SERVICE_PERSONNE(
  idPersonne VARCHAR,
  idService INT,
  PRIMARY KEY (idPersonne,idService),
  FOREIGN KEY (idPersonne) REFERENCES PERSONNE(login),
  FOREIGN KEY (idService) REFERENCES SERVICE(id)
);

CREATE TABLE SF_PERSONNE(
  idPersonne VARCHAR,
  idSF INT,
  degre INT,
  PRIMARY KEY (idPersonne,idSF),
  FOREIGN KEY (idPersonne) REFERENCES PERSONNE(login),
  FOREIGN KEY (idSF) REFERENCES SAVOIR_FAIRE(id),
  CONSTRAINT CHK_ServicePersonne CHECK (degre =1 OR degre = 2 OR degre = 3 OR degre = 4 OR degre =5)
);

CREATE TABLE LIEN_PERSONNE(
      idPersonne VARCHAR,
      idCible VARCHAR,
      description VARCHAR (50),
      PRIMARY KEY (idPersonne,idCible),
      FOREIGN KEY (idPersonne) REFERENCES PERSONNE(login),
      FOREIGN KEY (idCible) REFERENCES PERSONNE(login),
      CONSTRAINT CHK_LienPersonne CHECK (idPersonne!=idCible)
);


CREATE TABLE SERVICE_COMMUNAUTE(
  idCommunaute VARCHAR,
  idService INT,
  PRIMARY KEY (idCommunaute,idService),
  FOREIGN KEY (idCommunaute) REFERENCES COMMUNAUTE(login),
  FOREIGN KEY (idService) REFERENCES SERVICE(id)
);

CREATE TABLE SF_COMMUNAUTE(
  idCommunaute VARCHAR,
  idSF INT,
  degre INT,
  PRIMARY KEY (idCommunaute,idSF),
  FOREIGN KEY (idCommunaute) REFERENCES Communaute(login),
  FOREIGN KEY (idSF) REFERENCES SAVOIR_FAIRE(id),
  CONSTRAINT CHK_ServiceCommunaute CHECK (degre =1 OR degre = 2 OR degre = 3 OR degre = 4 OR degre =5)
);

CREATE TABLE LIEN_COMMUNAUTE(
      idCommunaute VARCHAR,
      idCible VARCHAR,
      description VARCHAR (50),
      PRIMARY KEY (idCommunaute,idCible),
      FOREIGN KEY (idCommunaute) REFERENCES COMMUNAUTE(login),
      FOREIGN KEY (idCommunaute) REFERENCES COMMUNAUTE(login),
      CONSTRAINT CHK_LienCommunaute CHECK (idCommunaute!=idCible)
);

/*INSERT*/
INSERT INTO COMPTE(cle) VALUES ('lblbzuoe'), ('ihpzodwx'), ('acbngfes'), ('kmjqqxza');

INSERT INTO PERSONNE(login, nom, prenom, anniversaire, longitude, latitude, compteG1) VALUES
  ('idarouic', 'DAROUICH', 'Inès', '1999-12-24', '2.796364', '49.39715', 'ihpzodwx'),
  ('dbakaral', 'BAKARALY', 'Danish', '1999-12-23', '2.79469', ' 49.398594', 'kmjqqxza'),
  ('malemoin', 'LEMOINE', 'Manon', '1999-03-16', '-25.58609', '50.89791', NULL),
  ('ajorgens', 'JORGENSEN', 'Alexandre', '1998-12-29', '126.28104', ' -22.68808', NULL),
  ('leformax', 'LEFORT', 'Maxence', '1999-09-28', '-159.10875', '-47.38820', 'acbngfes');

INSERT INTO COMMUNAUTE (login, nom, description, longitude, latitude, compteG1) VALUES
  ('sweat', 'The Sweatest', 'A super nice community', '79.95299', '-29.44813', NULL),
  ('genius', 'The Genius', 'A really smart community', '64.20556', '-17.96637', 'lblbzuoe');

INSERT INTO MEMBRE(idPersonne, idCommunaute) VALUES
  ('idarouic', 'genius'),
  ('idarouic', 'sweat'),
  ('ajorgens', 'sweat'),
  ('ajorgens', 'genius'),
  ('malemoin', 'genius'),
  ('dbakaral', 'sweat'),
  ('leformax', 'genius');

INSERT INTO VOTE VALUES
  ('idarouic', 'genius', 'leformax', 'genius'),
  ('ajorgens', 'sweat', 'idarouic', 'sweat');

INSERT INTO MESSAGE(objet, contenu, expediteurP, destinataireP, expediteurC, destinataireC, reference) VALUES
  ('Welcome', 'Dear Alexandre...', 'dbakaral', 'ajorgens', NULL, NULL, NULL),
  ('Nice to meet you', 'Dear Danish...', 'ajorgens', 'dbakaral', NULL, NULL, 1),
  ('Hello', '...', NULL, NULL, 'sweat', 'genius', NULL);

INSERT INTO SAVOIR_FAIRE(nom) VALUES('matériel informatique'), ('conduite'), ('jardinage'), ('enseignement');
INSERT INTO SERVICE(nom, description, type, prix, service) VALUES('cours à distance', '', 'sc', NULL, NULL), ('réparation appareil', '', 'm', 5, NULL), ('covoiturage', '', 's', 3, NULL);
INSERT INTO SF_SERVICE(idSF, idService) VALUES (2, 3), (4, 1), (1, 2);
INSERT INTO SERVICE_PERSONNE(idPersonne, idService) VALUES ('ajorgens', 2), ('idarouic', 3);
INSERT INTO SF_PERSONNE(idPersonne, idSF, degre) VALUES ('ajorgens', 1, 2), ('idarouic', 2, 3), ('dbakaral', 3, 4);
INSERT INTO SF_COMMUNAUTE(idCommunaute, idSF, degre) VALUES ('sweat', 4, 4), ('sweat', 3, 3), ('genius', 1, 2);
INSERT INTO SERVICE_COMMUNAUTE(idCommunaute, idService) VALUES
  ('sweat', 3),
  ('sweat', 2),
  ('genius', 1),
  ('genius', 2);

INSERT INTO LIEN_PERSONNE(idPersonne, idCible, description) VALUES
  ('leformax', 'ajorgens', 'habite près'),
  ('ajorgens', 'dbakaral', 'dans la même classe'),
  ('dbakaral', 'ajorgens', 'dans la même classe');

INSERT INTO LIEN_COMMUNAUTE(idCommunaute, idCible, description) VALUES
  ('sweat', 'genius', 'amicale'),
  ('genius', 'sweat', 'défie');

  /* VUES */

CREATE OR REPLACE VIEW vMessages(ID,Objet,  Contenu, Date, Expediteur, Destinataire, Reference) AS
         SELECT M1.id,
         M1.objet,
         M1.contenu,
         M1.dateEnvoi,
         CASE
          WHEN M1.expediteurP IS NULL THEN M1.expediteurC
          ELSE  M1.expediteurP
         END,
         CASE
          WHEN M1.destinataireP IS NULL THEN M1.destinataireC
          ELSE  M1.destinataireP
         END,
         M1.reference
         FROM MESSAGE M1
         ;

CREATE OR REPLACE VIEW vFait_Partie(Identifiant, Nom, Prenom, Communaute, Exclu) AS
         SELECT M.idPersonne as Identifiant,
         P.nom,
         P.prenom,
         C.nom,
         M.exclu
         FROM MEMBRE M
         INNER JOIN PERSONNE P ON M.idPersonne = P.login
         INNER JOIN COMMUNAUTE C ON M.idCommunaute = C.login;

  CREATE OR REPLACE VIEW vUtilisateurs(login, latitude ,longitude, type)
  AS
  SELECT login, latitude, longitude, 'Communaute'
  FROM COMMUNAUTE
  UNION
  SELECT login, latitude, longitude , 'Personne'
  FROM PERSONNE;

  CREATE OR REPLACE VIEW vProches(Utilisateur1,Type1,Utilisateur2,Type2,distance)
  AS
  WITH tmpTable AS ( SELECT U1.login,
                                                  U1.type,
                                                  U2.login,
                                                  U2.type,
                                                  6371*ACOS(cast(SIN(RADIANS(U1.latitude))*SIN(RADIANS(U2.latitude)) + COS(RADIANS(U1.latitude))*COS(RADIANS(U2.latitude))*COS(RADIANS(U2.longitude - U1.longitude)) as numeric)) as distance
                                      FROM vUtilisateurs U1, vUtilisateurs U2

                                    WHERE U1.login <> U2.login

                                )
  SELECT * FROM tmpTable WHERE (tmpTable.distance < 1);

  CREATE OR REPLACE VIEW vLocalisation(Utilisateur, Type, Localisation)
  AS
  SELECT U.login, U.type, 'https://www.openstreetmap.org/#map=17/' || U.latitude || '/' || U.longitude
  FROM vUtilisateurs U;
