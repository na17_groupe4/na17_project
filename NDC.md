# Note de Clarification du projet Résilience (groupe 4)

## Description du projet

L'objectif de ce projet est de mettre en réseau des personnes et des communautés.

Une personne pourra faire partie d'une communauté et avoir des liens avec d'autres personnes. Deux communautés pourront également avoir des liens. 

Une personne aura la possibilité d'administrer les informations de la communauté mais également s'opposer à la présence d'une autre personne. 

Un utilisateur (personne ou communauté) aura  la possibilité d'avoir des savoir-faire et, pour une personne, chaque communauté à laquelle elle appartient pourra bénéficier de celui-ci.

Un utilisateur du projet pourra proposer des services, avec ou sans contrepartie. S'il souhaite avoir une contrepartie, il pourra demander soit un autre service (identifié) soit une somme en monnaie Ğ1. Les utilisateurs pourront également s'échanger des messages entre eux (à destinataire unique) et faire référence à un autre message.

On aura également la possibilité de localiser une personne avec ses coordonnées géographiques.


## Liste des objets nécessaires à la modélisation
**Personne**
- possède un nom et prénom (hypothèse)
- peut avoir des liens unidirectionnels avec d'autres personnes
- peut déclarer posséder un savoir faire avec un certain degré (entre 1 et 5)
- peut proposer un service
- peut faire partie de 0 à N communautés
- possède un compte Ğ1
- possède des cordoonées (latitude, longitude, zoom)
- possède deux états relatifs à une communauté : exclu ou non exclu de cette communauté


**Communautés**
- possède un nom (hypothèse unique)
- peut avoir des liens unidirectionnels avec d'autres communautés
- peut avoir un lien avec un savoir-faire avec un certain degré (entre 1 et 5)
- possède un compte Ğ1
- possède des cordoonnées géographiques (latitute, longitude, zoom)

**Lien**
- possède une description

**Message** 
- possède un objet et un contenu (hypothèse)
- peut faire référence à un autre message
- est envoyé à un individu ou une communauté
- est envoyé par un individu ou une communauté

**Compte**
- est possédé par un individu ou une communauté
- possède une clé publique
- possède un solde (hypothèse)


**Service**
- possède un nom (hypothèse)
- possède une description
- peut être lié à des savoir-faire
- peut être de type :
    - sans contrepartie
    - avec contrepartie :
        - en contrepartie d'un autre service (sera lié à ce service)
        - en contrepartie d'un montant (possèdera un prix en Ğ1)

**Savoir-faire**
- possède un nom
- peut être lié à des services
- peut être lié à un utilisateur (communauté ou personne) avec un degré

## Contraintes

- Un montant en Ğ1 sera nécessairement positif

## Utilisateurs de l'application


Dans ce projet, lorsque l'on parlera d'un utilisateur, nous ferons référence à une communauté ou bien une personne.

Cependant, une communauté et une personne n'auront pas les mêmes droits et donc nous identifierons deux profils : personne et communauté.



## Droits des utilisateurs

**Droits communs** (personne et communauté) :
- Peut visualiser chaque message en ajoutant l'identifiant du message d'origine lorsque le message s'inscrit dans un fil historique
- Peut visualiser les communautés et personnes proches de chaque personne et communauté (à une distance inférieure à 1km)
    - Peut proposer un service
    - Peut proposer un savoir-faire
    - Peut envoyer un message et faire référence à un autre message
    - Peut créer une relation avec un autre Utilisateur
    
**Personne** :
-  Peut avoir la liste des communautés auxquelles il déclare appartenir avec un booléen qui détermine si la personne est exclue ou non.
- Peut voter pour l'exclusion d'un membre de la communauté
- Peut administrer les communautés dont il est membre

## Hypothèses faites pour la modélisation
    H0 : Une communauté au même titre qu'une personne possède des coordonnées géographiques
    H1 : Une communauté ou un utilisateur ne possède qu'un seul compte Ğ1
    H2 : Deux personnes ne peuvent pas avoir le même nom et prénom 
    H3 : Deux communautés ne peuvent pas avoir le même nom
    H4 : Deux comptes Ğ1 ne peuvent pas avoir la même clé publiques
    H5 : Deux savoir-faire ne peuvent pas avoir le même nom
    H6 : Deux services ne peuvent pas avoir le même nom
    H7 : Un utilisateur n'ayant pas assez d'argent sur son compte Ğ1 ne pourra pas utiliser un service contre monnaie
    H8 : Lors de l'exclusion d'une personne d'une communauté, la relation existe toujours et l'état de cette personne vis à vis de la communauté passe à "exclu"

## Données de test
Tous les étudiants utilisés dans la base de données sont consentants et indépendants de sa réalisation.
