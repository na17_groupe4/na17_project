# NA17_Project  Walckiers  Kerskens  Ben Salem


Git du Groupe 4 du projet de NA17 "Résilience".

URL du projet : https://gitlab.utc.fr/na17_groupe4/na17_project

**Livrables**
*   README.md (avec les noms des membres du groupe)
*   NDC au format markdown
*   MCD
*   MLD non-relationnel
*   BBD : base de données, données de test, questions
*   Application Python

**Membres du Groupe 4**
*   Jinane Ben Salem
*   Amaury Walckiers
*   Valentin Kerskens